from flask import render_template, flash, redirect, session, url_for, request, g, Markup
import json
from app import app
from posapi import PosApi


@app.route('/')
def health():
    return "ebarimt service is running healthy!"


@app.route('/checkApi')
def checkApi():
    result = PosApi().checkApi()
    return result


@app.route('/getInformation')
def getInformation():
    result = PosApi().getInformation()
    return result


@app.route('/callFunction', methods=['POST'])
def callFunction():
    functionName = request.json['functionName']
    str_functionName = json.dumps(functionName)

    data = request.json['data']
    str_data = json.dumps(data)
    result = PosApi().callFunction(
        str_functionName.encode('ascii'), str_data.encode('ascii'))

    return result


@app.route('/put', methods=['POST'])
def put():
    data = request.json['data']
    str_data = json.dumps(data)
    result = PosApi().put(str_data.encode('ascii'))
    return result


@app.route('/returnBill', methods=['POST'])
def returnBill():
    data = request.json['data']
    str_data = json.dumps(data)
    result = PosApi().returnBill(str_data.encode('ascii'))
    return result


@app.route('/sendData')
def sendData():
    result = PosApi().sendData()
    return result
